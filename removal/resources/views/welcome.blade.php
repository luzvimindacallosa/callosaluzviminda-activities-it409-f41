<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Argie Music App</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token()}}">
  <meta name="url" content="{{ url('')}}">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


        <!-- Styles -->
        <link rel="stylesheet" href="./css/mystyle.css">

    </head>
    <body>
    <div id="app">
    <nav class="menu" tabindex="0" >
    <div class="smartphone-menu-trigger"></div>
    <header class="avatar">
      <img src="img/logo.png">
      <h2>MUSIKA</h2>
    </header>
    <ul>
      <li tabindex="0" data-toggle="modal" data-target="#myModal2" class="icon-upload"><span>Upload Music</span></li>
      <li tabindex="0" class="icon-allSongs"><span>All songs</span></li>
      <li tabindex="0" data-toggle="modal" data-target="#myModal" class="icon-createPlaylist"><span>Create Playlist</span></li>
    </ul>
    <div id="navApp">

      <ul>
        <li tabindex="0" >
          <ul>
            <li  tabindex="0" class="icon-bullet"><span>Playlist name</span></li>
            
          </ul>
        </li>
      </ul>


      <!-- Modal -->
      <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
        
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Create Playlist</h4>
            </div>
            <div class="modal-body">
                <input type="text" id="create_playlist" v-model="new_playlist" />
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" @click="addNewPlaylist" data-dismiss="modal">Create</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>
          
        </div>
      </div>

    </div>
    
  </nav>

  <main>
      @yield('content')
    <div class="myDiv">
      <input type="text" id="search" name="search" value="Search for songs, artist, etc...">
      <h2 class="allSongsLabel">All songs</h2>
      <table class="myTable" border="1" style="border-collapse: collapse; border: 1px solid black;">
        <thead>
          <tr>
            <td style="width: 500px;">
              Title
            </td>
            <td style="width: 500px;">
              Artist
            </td>
            <td style="width: 500px;">
              Album
            </td>
            <td style="width: 500px;">
              Duration
            </td>
          </tr>

        </thead>

        <tbody>
          <tr >
            <td style="width: 900px;">
              <img src="img/triangle.png" /> Song title
            </td>
            <td>
              artist name
            </td>
            <td>
              album name
            </td>
            <td>
              03:24
            </td>
          </tr>
         

        </tbody>

      </table>
      <div class="progress">
        <div class="played">
          <div class="circle"></div>
        </div>
      </div>
    </div>
    <div>
      <table style="margin: 0 auto; margin-top: 40px;">

        <tr>

          <td>
            <button class="btn"><i class="fa fa-backward"></i></button>
          </td>
          <td style="padding: 10px;">
            <button class="btn"><i class="fa fa-play"></i></button>
          </td>
          <td>
            <button class="btn"><i class="fa fa-forward"></i></button>
          </td>
        </tr>
      </table>


    </div>
  </main>

  <div id="myModal2" class="modal">


    <div class="modal-content">
      <span class="close">&times;</span>
      <div style="height: 50px; width: 500px; background-color: #4E0746;"><span
          style="float:left;font-size: 18px; margin-top: 10px; margin-left: 10px;">Upload Music</span></div>

      <div
        style="height: 80px; width: 400px; background-color: #b9b4b4; margin: 0 auto; border-radius: 10px; margin-top: 10px">
        <span style="float:left;font-size: 18px; margin-top: 30px; margin-left: 60px; color:gray">Drag and drop files
          here.</span>

          <template>
            <div id="file-drag-drop">
              <form ref="fileform">
                  <span class="drop-files">Drop the files here!</span>
              </form>
            </div>
          </template>
      </div>
      <div style="width: 200px; margin-top: 10px; float: right;">
        <button class="button" >Upload</button>
        <button class="button button_cancel" data-dismiss="modal">Cancel</button>
      </div>

    </div>

  </div>
  </div> <!-- end of id app -->

  <script src="{{ asset('js/app.js') }}"></script>
  
</body>
</html>
