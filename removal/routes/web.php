<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Songscontroller;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});





Route::post('/uploadsong', [Songscontroller::class, 'uploadsong']);
Route::get('/displaysong', [Songscontroller::class, 'displaysong']);
Route::post('/deletemusic', [Songscontroller::class, 'deletemusic']);




Route::post('/createplaylist', [Songscontroller::class, 'createplaylist']);
Route::get('/displayplaylist', [Songscontroller::class, 'displayplaylist']);
Route::post('/deleteplaylist', [Songscontroller::class, 'deleteplaylist']);
Route::post('/editplaylist', [Songscontroller::class, 'editplaylist']);


