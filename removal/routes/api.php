<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use app\Http\Controllers\SongsController;
use app\Http\Controllers\TodoController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/list', 'TodoController@index');
Route::get('/list/{id}', 'TodoController@show_task_list');
Route::get('/list/{id}/{completed}', 'TodoController@show_completed_task_list');
Route::post('/create_list', 'TodoController@create_list');

Route::get('/task', 'TodoController@showtask');
Route::post('/create_task', 'TodoController@create_task');

Route::put('/edit_task/{id}', 'TodoController@edit_task');

