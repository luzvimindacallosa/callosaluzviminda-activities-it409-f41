<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Song;
use App\Todo_list;
use App\Playlist;

class SongsController extends Controller
{
    public function index(){
        // $songs = DB::select('select * from songs');
        // return ['songs'=>$songs];
        $todo_list = Todo_list::all();
        return $todo_list;
    }

    public function uploadSong($title,$length,$artist,$created_at,$updated_at){
        $data=array('title'=>$playlistName,"length"=>$length,"artist"=>$artist,"created_at"=>$created_at,"updated_at"=>$updated_at);
        DB::table('songs')->insert($data);
        echo "song inserted successfully.<br/>";
    }

    public function createplaylist(){
    	$playlist = new Playlist();
        $playlist->name = request('name');
        $playlist->save();
        return $playlist->id;
    }

    public function store(Request $request){
        $newSong = new Song;
        $newSong->title = $request->song["title"];
        $newSong->length = $request->song["length"];
        $newSong->artist = $request->song["artist"];

        $newSong->save();

        return $newSong;


    }
}
