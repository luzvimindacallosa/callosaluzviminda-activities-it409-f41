<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Todo_list;
use App\Todo_task;

class TodoController extends Controller
{
    public function index(){
        $list = Todo_list::all();
        return $list;
    }
    public function create_list(Request $request){
        $list = new Todo_list();
        $list->list = $request->list;
        $list->save();
        $id = $list->id;
        return $id;
    }
    public function showtask(){
        $task = Todo_task::all();
        return $task;
    }
    public function show_task_list($id){
        $task = Todo_task::where('list_id', $id)->get();
        return ['task'=>$task];
    }
    public function show_completed_task_list($id, $completed){
        $task = DB::select('select * from todo_tasks where list_id = '.$id.' and completed = '.$completed.' ');
        return ['task'=>$task];
    }
    public function edit_task($id, $completed){
        $task = DB::select('UPDATE todo_tasks set completed = ? where id = ?', [$completed,$id]);
        return ['task'=>$task];
    }
    public function create_task(Request $request){
        $task = new Todo_task();
        $task->list_id = $request->list_id;
        $task->task = $request->task;
        $task->completed = 0;
        $task->save();
        $id = $task->id;
        return $id;
    }
}
